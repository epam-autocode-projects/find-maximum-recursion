﻿using System;

namespace FindMaximumTask
{
    /// <summary>
    /// Class for operations with array.
    /// </summary>
    public static class ArrayExtension
    {
        /// <summary>
        /// Finds the element of the array with the maximum value recursively.
        /// </summary>
        /// <param name="array"> Source array. </param>
        /// <returns>The element of the array with the maximum value.</returns>
        /// <exception cref="ArgumentNullException">Thrown when array is null.</exception>
        /// <exception cref="ArgumentException">Thrown when array is empty.</exception>
        public static int FindMaximum(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException("bad one");
            }

            return SortF(array, 0, array.Length - 1);
        }

        public static int SortF(int[] array, int i, int j)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException("bad one");
            }

            int localmax;
            int mid, max1, max2;

            if (i == j)
            {
                localmax = array[i];
            }
            else
            {
                mid = (i + j) / 2;
                int result1 = SortF(array, i, mid);
                int result2 = SortF(array, mid + 1, j);
                max1 = result1;
                max2 = result2;
                if (max1 < max2)
                {
                    localmax = max2;
                }
                else
                {
                    localmax = max1;
                }
            }

            return localmax;
        }
    }
}
